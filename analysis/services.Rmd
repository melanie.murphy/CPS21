---
title: "Services and Products"
date: "3/1/2021"
output: 
  workflowr::wflow_html:
    code_folding: hide
editor_options:
  chunk_output_type: console
---


This section reviews the survey frequencies for the quality, timeliness, and delivery of CPS services and products. 

#####

```{r global_options, include=F, warning=F, message=F, echo=F, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=10, fig.height=8, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=F)

source("code/CPS workflowr prep.R")

```

```{r, echo = F}
#Importing all the data

#First file is only 2021 data
dat <- read_dta("data/prepared/CPSSurveyResults2021-prepared.dta")
datNames <- data.frame(names(dat))

#datL contains all merged data
datL <- read_dta("data/prepared/CPSSurveyResultsmerged-prepared.dta")
datLNames <- data.frame(names(datL))

#create a set of only 2021 data
datL_21 <- datL %>%
  filter(year == 2021)

```

## Services and Products Questions

```{r, echo = FALSE}
#Generate frequencies for all the perception questions 
#This is only a reference point to confirm that later code is correct
dat_serv <- dat %>%
      dplyr::select(starts_with("serv") & ends_with("agree")) %>%
  frq()

#dat_serv  


#Generate a vector of short names to include in the Agree column of the objects below
short_names <- data.frame(short = c("Office delivers relevant and timely services"
                 , "It's easier for me to provide high quality services"
                 , "It's easier for CPS to provide high quality services"
                 , "My work is field-focused"
                 , "Ensure we provide necessary support to Missions"
                 , "Creative approaches are embraced by leadership"
                 , "My colleagues are willing to take risks"
                 , "My manager allows me to take risks")
                 , question_serv = c("serv_add_agree", 
                                    "serv_eas_agree", 
                                    "serv_eas_CPS_agree", 
                                    "serv_field_agree", 
                                    "serv_miss_agree", 
                                    "serv_crea_agree", 
                                    "serv_oth_risk_agree", 
                                    "serv_me_risk_agree")) 



#Generate the individual question tables


#Here's a function that works for these!
frq_fun <- function (variable, title) {
  dat %>%
  select(!!variable) %>%
  filter(!is.na(!!variable)) %>%
  summarise(Agree = mean(!!variable, na.rm=T),
            Total = n()) %>%
  na.omit() %>%
  mutate(Question=title) %>%
  select(Question, Agree, Total)
}

#Apply function to all the questions and insert the titles
q2.10 <- frq_fun(quo(serv_add_agree)
                   , title = "Office delivers relevant and timely services")

q2.20 <- frq_fun(quo(serv_eas_agree)
                     , title = "It's easier for me to provide high quality services")

q2.30 <- frq_fun(quo(serv_eas_CPS_agree)
                     , title = "It's easier for CPS to provide high quality services")

q2.40 <- frq_fun(quo(serv_field_agree)
                     , title = "My work is field-focused")

q2.50 <- frq_fun(quo(serv_miss_agree)
                     , title = "Ensure we provide necessary support to Missions")

q2.60 <- frq_fun(quo(serv_crea_agree)
                     , title = "Creative approaches are embraced by leadership")

q2.70 <- frq_fun(quo(serv_oth_risk_agree)
                     , title = "My colleagues are willing to take risks")

q2.80 <- frq_fun(quo(serv_me_risk_agree)
                     , title = "My manager allows me to take risks")


#Bind q2.10:q2.80 to make a single table with all of the above outputs    
q2 <- do.call(rbind, list(q2.10, q2.20, q2.30, q2.40, q2.50, q2.60, q2.70, q2.80))

```

```{r, echo = F}
#Generate a nice looking table
q2_gt <- q2 %>%
  gt() %>%
  gt::fmt_percent(2, decimals=1) %>%
  gt::tab_header("Quality, Timeliness, and Delivery of CPS Services and Produces")

q2_gt

#gtsave(q2_gt, "output/tables/Q2 CPS services and products.rtf")

```

## Year-on-Year plots on Services and Products Questions

```{r Year-on-Year Change on Perception Questions, include = FALSE}

frq(datL$year)

#Function to show year-on-year change in percentages

frq_year_fun <- function (variable, title) {
  datL %>%
    group_by(year) %>%
    filter(!is.na(!!variable)) %>%
    summarise(cult_mn = mean(!!variable)) %>%
    mutate(Agree = (title))
    
}

#Generate the individual year on year change for each of the questions
#these will then be passed to ggplot to make charts
q2.10_ov <- frq_year_fun(quo(serv_add_agree)
                   , title = "Office delivers relevant and timely services")

q2.20_ov <- frq_year_fun(quo(serv_eas_agree)
                     , title = "It's easier for me to provide high quality services")

q2.30_ov <- frq_year_fun(quo(serv_eas_CPS_agree)
                     , title = "It's easier for CPS to provide high quality services")

q2.40_ov <- frq_year_fun(quo(serv_field_agree)
                     , title = "My work is field-focused")

q2.50_ov <- frq_year_fun(quo(serv_miss_agree)
                     , title = "Ensure we provide necessary support to Missions")

q2.60_ov <- frq_year_fun(quo(serv_crea_agree)
                     , title = "Creative approaches are embraced by leadership")

q2.70_ov <- frq_year_fun(quo(serv_oth_risk_agree)
                     , title = "My colleagues are willing to take risks")

q2.80_ov <- frq_year_fun(quo(serv_me_risk_agree)
                     , title = "My manager allows me to take risks")

```

```{r, echo = FALSE}

plot_year_fun <- function (data, title) {

ggplot(data, aes(year, cult_mn)) +
  geom_line(size = 1, color = "dodgerblue") +
  scale_y_continuous(limits = c(0.2, .9), 
                     labels = percent_format(accuracy = 1)) +
  scale_x_continuous(breaks = seq(2020, 2021), 
                     labels = c(2020, 2021),
                     expand = expansion(mult = c (.3, .3))) +
  geom_point(size = 3, color = "dodgerblue") +
  labs(x = "", y = "", title = title) +
  theme(plot.title = element_text(size = 12, face = "bold")) +
  geom_text_repel(min.segment.length = 1, 
                  box.padding = .6, 
                  size = 6, 
                  color = "dodgerblue", 
                  nudge_x = -.1, 
                  nudge_y = .1,
                  aes(label = paste(round(cult_mn*100, 1), "%", sep = "")))
}


pq2.10_ov <- plot_year_fun(q2.10_ov
                         , title = "Office delivers\nrelevant and timely services")

pq2.20_ov <- plot_year_fun(q2.20_ov
                     , title = "It's easier for me to\nprovide high quality services")

pq2.30_ov <- plot_year_fun(q2.30_ov
                     , title = "It's easier for CPS to\nprovide high quality services")

pq2.40_ov <- plot_year_fun(q2.40_ov
                     , title = "My work is\nfield-focused")

pq2.50_ov <- plot_year_fun(q2.50_ov
                     , title = "Ensure we provide\nsupport to Missions")

pq2.60_ov <- plot_year_fun(q2.60_ov
                     , title = "Creative approaches\nare embraced by leadership")

pq2.70_ov <- plot_year_fun(q2.70_ov
                     , title = "My colleagues are\nwilling to take risks")

pq2.80_ov <- plot_year_fun(q2.80_ov
                     , title = "My manager allows\nme to take risks")

plots_ser <- plot_grid(pq2.10_ov
                      , pq2.20_ov
                      , pq2.30_ov
                      , pq2.40_ov
                      , pq2.50_ov
                      , pq2.60_ov
                      , pq2.70_ov
                      , pq2.80_ov
                      , align = "h")

plots_ser

#save_plot("output/viz/Q2plots CPS services and products by year.png", plot = plots_ser, device = "png", type = "cairo")
```
