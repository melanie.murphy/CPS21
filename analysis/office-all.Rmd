---
title: "Office Functionality"
output: 
  workflowr::wflow_html:
    code_folding: hide
editor_options:
  chunk_output_type: console
---

This section reviews the overall survey frequencies and demographic frequencies across each survey item on how CPS offices function internally.

#####

```{r global_options, include=F, warning=F, message=F, echo=F, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=10, fig.height=8, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=F)

source("code/CPS workflowr prep.R")

```

```{r, echo = F}
#Importing all the data

#First file is only 2021 data
dat <- read_dta("data/prepared/CPSSurveyResults2021-prepared.dta")
datNames <- data.frame(names(dat))

#datL contains all merged data
datL <- read_dta("data/prepared/CPSSurveyResultsmerged-prepared.dta")
datLNames <- data.frame(names(datL))

#create a set of only 2021 data
datL_21 <- datL %>%
  filter(year == 2021)

```

## Internal Functionality Questions

# Question 3 Frequencies 
The table below shows the percentage of respondents who agree or strongly agree with the list of questions provided in question 3. 
```{r, echo = FALSE}
#Generate frequencies for all the perception questions 
#This is only a reference point to confirm that later code is correct
dat_off <- dat %>%
      dplyr::select(starts_with("int") & ends_with("agree")) %>%
  frq()

#dat_off 


#Generate a vector of short names to include in the Agree column of the objects below
short_names <- data.frame(short = c("Manager promotes collaboration across offices"
                 , "Trust between myself and colleagues within my office"
                 , "Colleagues in other offices trust me"
                 , "Understand the day-to-day functions of other offices within CPS"
                 , "Feel that decisions within CPS are taken in a timely fashion"
                 , "Feel that decisions are made effectively and with transparency"
                 , "New processes reduce the time I spend on activities that do not add value")
                 , question_int = c("int_collab_agree",
                                    "int_trust_agree",
                                    "int_tru_b_agree",
                                    "int_under_agree",
                                    "int_time_agree",
                                    "int_eff_agree",
                                    "int_proc_agree")) 

#Generate the individual question tables

#Here's a function that works for these!
frq_fun <- function (variable, title) {
  dat %>%
  select(!!variable) %>%
  filter(!is.na(!!variable)) %>%
  summarise(Agree = mean(!!variable, na.rm=T),
            Total = n()) %>%
  na.omit() %>%
  mutate(Response=title) %>%
  select(Response, Agree, Total)
}

#Apply function to all the questions and insert the titles
q3.10 <- frq_fun(quo(int_collab_agree)
                   , title = "Manager promotes collaboration across offices")

q3.20 <- frq_fun(quo(int_trust_agree)
                     , title = "Trust between myself and colleagues within my office")

q3.30 <- frq_fun(quo(int_tru_b_agree)
                     , title = "Colleagues in other offices trust me")

q3.40 <- frq_fun(quo(int_under_agree)
                     , title = "Understand the day-to-day functions of other offices within CPS")

q3.50 <- frq_fun(quo(int_time_agree)
                     , title = "Feel that decisions within CPS are taken in a timely fashion")

q3.60 <- frq_fun(quo(int_eff_agree)
                     , title = "Feel that decisions are made effectively and with transparency")

q3.70 <- frq_fun(quo(int_proc_agree)
                     , title = "New processes reduce time spent on activities that do not add value")

#Bind q3.10:q3.70 to make a single table with all of the above outputs    
q3 <- do.call(rbind, list(q3.10, q3.20, q3.30, q3.40, q3.50, q3.60, q3.70))

```

```{r, echo = F}
#Generate a nice looking table
q3_gt <- q3 %>%
  gt() %>%
  gt::fmt_percent(2, decimals=1) %>%
  gt::tab_header("Internal Functioning at CPS")

q3_gt

gtsave(q3_gt, "output/tables/Q3 CPS internal functioning of CPS.rtf")

```

## Year-on-Year plots on Internal Functionality Questions
The following plots show the percentage of respondents who agree or strongly agree to the internal office functionality questions in 2020 and in 2021. 

```{r Year-on-Year Change on Internal Functioning Questions, include = FALSE}

frq(datL$year)

#Function to show year-on-year change in percentages

frq_year_fun <- function (variable, title) {
  datL %>%
    group_by(year) %>%
    filter(!is.na(!!variable)) %>%
    summarise(cult_mn = mean(!!variable)) %>%
    mutate(Agree = (title))
    
}

#Generate the individual year on year change for each of the questions
#these will then be passed to ggplot to make charts
q3.10_ov <- frq_year_fun(quo(int_collab_agree)
                   , title = "Manager promotes collaboration across offices")

q3.20_ov <- frq_year_fun(quo(int_trust_agree)
                     , title = "Trust between myself and colleagues within my office")

q3.30_ov <- frq_year_fun(quo(int_tru_b_agree)
                     , title = "Colleagues in other offices trust me")

q3.40_ov <- frq_year_fun(quo(int_under_agree)
                     , title = "Understand the day-to-day functions of other offices within CPS")

q3.50_ov <- frq_year_fun(quo(int_time_agree)
                     , title = "CPS decisions are taken in a timely fashion")

q3.60_ov <- frq_year_fun(quo(int_eff_agree)
                     , title = "CPS decisions are effective and transparent")

q3.70_ov <- frq_year_fun(quo(int_proc_agree)
                     , title = "New processes are efficient")

```

```{r, echo = FALSE}

plot_year_fun <- function (data, title) {

ggplot(data, aes(year
                     , cult_mn)) +
  geom_line(size = 1
            , color = "#002F6C") +
  scale_y_continuous(limits = c(0.2, 1), 
                     labels = percent_format(accuracy = 1)) +
  scale_x_continuous(breaks = seq(2020, 2021), 
                     labels = c(2020, 2021),
                     expand = expansion(mult = c (.3, .3))) +
  geom_point(size = 3, color = "#002F6C") +
  labs(x = "", y = "", title = title) +
  theme(plot.title = element_text(size = 12, face = "bold")) +
  geom_text_repel(min.segment.length = 1, 
                  box.padding = .6, 
                  size = 6, 
                  color = "#002F6C", 
                  nudge_x = -.1, #Moved the labels a little
                  nudge_y = .1,
                  aes(label = paste(round(cult_mn*100, 1), "%", sep = "")))
}


pq3.10_ov <- plot_year_fun(q3.10_ov
                         , title = "Manager promotes\ncollaboration across offices")

pq3.20_ov <- plot_year_fun(q3.20_ov
                     , title = "Trust between myself and\ncolleagues within my office")

pq3.30_ov <- plot_year_fun(q3.30_ov
                     , title = "Colleagues in other\noffices trust me")

pq3.40_ov <- plot_year_fun(q3.40_ov
                     , title = "Understand the day-to-day functions\nof other offices within CPS")

pq3.50_ov <- plot_year_fun(q3.50_ov
                     , title = "CPS decisions are\ntaken in a timely fashion")

pq3.60_ov <- plot_year_fun(q3.60_ov
                     , title = "CPS decisions are\neffective and transparent")

pq3.70_ov <- plot_year_fun(q3.70_ov
                     , title = "New processes\nare efficient")

plots_off <- plot_grid(pq3.10_ov
                      , pq3.20_ov
                      , pq3.30_ov
                      , pq3.40_ov
                      , pq3.50_ov
                      , pq3.60_ov
                      , pq3.70_ov
                      , align = "h")

plots_off

#save_plot("output/viz/Q3plots internal functioning by year.png", plot = plots_off, device = "png", type = "cairo")
```


```{r}
#getting the overall frequencies of Q3.1-3.7 then will do a function do collect the remaining objects needed 

ind3.1_ov <- dat %>%
  summarise(Overall = mean(int_collab_agree, na.rm=T)) %>%
  mutate(Question = "3.1") %>%
  select(2,1)

ind3.2_ov <- dat %>%
  summarise(Overall = mean(int_trust_agree, na.rm=T)) %>%
  mutate(Question = "3.2") %>%
  select(2,1)

ind3.3_ov <- dat %>%
  summarise(Overall = mean(int_tru_b_agree, na.rm=T)) %>%
  mutate(Question = "3.3") %>%
  select(2,1)

ind3.4_ov <- dat %>%
  summarise(Overall = mean(int_under_agree, na.rm=T)) %>%
  mutate(Question = "3.4") %>%
  select(2,1)

ind3.5_ov <- dat %>%
  summarise(Overall = mean(int_time_agree, na.rm=T)) %>%
  mutate(Question = "3.5") %>%
  select(2,1)

ind3.6_ov <- dat %>%
  summarise(Overall = mean(int_eff_agree, na.rm=T)) %>%
  mutate(Question = "3.6") %>%
  select(2,1)

ind3.7_ov <- dat %>%
  summarise(Overall = mean(int_proc_agree, na.rm=T)) %>%
  mutate(Question = "3.7") %>%
  select(2,1)


loc_fun <- function (variable) {

  dat %>%
  group_by(loc_prim_en) %>% 
  summarise(prop=mean(!!variable, na.rm=T)) %>%
  mutate(labels = c("Field", "DC")) %>%
  select(2:3) %>%
  pivot_wider(names_from=labels,
              values_from=prop) 
}

ind3.1_loc <- loc_fun(quo(int_collab_agree))
ind3.2_loc <- loc_fun(quo(int_trust_agree))
ind3.3_loc <- loc_fun(quo(int_tru_b_agree))
ind3.4_loc <- loc_fun(quo(int_under_agree))
ind3.5_loc <- loc_fun(quo(int_time_agree))
ind3.6_loc <- loc_fun(quo(int_eff_agree))
ind3.7_loc <- loc_fun(quo(int_proc_agree))

sex_fun <- function (variable) {
dat %>%
  group_by(sex_en) %>%
  filter(sex_en==1|sex_en==2) %>% 
  summarise(prop=mean(!!variable, na.rm=T)) %>%
  mutate(labels = c("Male", "Female")) %>%
  select(2:3) %>%
  pivot_wider(names_from=labels,
              values_from=prop)
}

ind3.1_sex <- sex_fun(quo(int_collab_agree))
ind3.2_sex <- sex_fun(quo(int_trust_agree))
ind3.3_sex <- sex_fun(quo(int_tru_b_agree))
ind3.4_sex <- sex_fun(quo(int_under_agree))
ind3.5_sex <- sex_fun(quo(int_time_agree))
ind3.6_sex <- sex_fun(quo(int_eff_agree))
ind3.7_sex <- sex_fun(quo(int_proc_agree))

hir_fun <- function (variable) {

dat %>%
  filter(hir_mech2 == 1 | hir_mech2 == 2 | hir_mech2 == 3 | hir_mech2 == 4) %>%
  group_by(hir_mech2) %>%
  summarise(prop=mean(!!variable, na.rm=T)) %>%
  mutate(labels = c("CS", "FS/SFS/FSL", "ISC", "USPSC")) %>%
  select(2:3) %>%
  pivot_wider(names_from=labels,
              values_from=prop) 

}

ind3.1_hir <- hir_fun(quo(int_collab_agree))
ind3.2_hir <- hir_fun(quo(int_trust_agree))
ind3.3_hir <- hir_fun(quo(int_tru_b_agree))
ind3.4_hir <- hir_fun(quo(int_under_agree))
ind3.5_hir <- hir_fun(quo(int_time_agree))
ind3.6_hir <- hir_fun(quo(int_eff_agree))
ind3.7_hir <- hir_fun(quo(int_proc_agree))

opu_fun <- function (variable) {

dat %>%
  group_by(op_unit2) %>%
  summarise(prop=mean(!!variable, na.rm=T)) %>%
  mutate(labels = c("AMS/FO", "CMC", "CVP", "OTI", "PO")) %>% 
  select(2:3) %>%
  pivot_wider(names_from=labels,
              values_from=prop)
}

ind3.1_opu <- opu_fun(quo(int_collab_agree))
ind3.2_opu <- opu_fun(quo(int_trust_agree))
ind3.3_opu <- opu_fun(quo(int_tru_b_agree))
ind3.4_opu <- opu_fun(quo(int_under_agree))
ind3.5_opu <- opu_fun(quo(int_time_agree))
ind3.6_opu <- opu_fun(quo(int_eff_agree))
ind3.7_opu <- opu_fun(quo(int_proc_agree))


ten_fun <- function (variable) {

dat %>%
  group_by(tenure_en) %>%
  summarise(prop=mean(!!variable, na.rm=T)) %>%
  mutate(labels = c("0-3", "3-5", "5-10", "10+")) %>% 
  select(2:3) %>%
  pivot_wider(names_from=labels,
              values_from=prop)
}

ind3.1_ten <- ten_fun(quo(int_collab_agree))
ind3.2_ten <- ten_fun(quo(int_trust_agree))
ind3.3_ten <- ten_fun(quo(int_tru_b_agree))
ind3.4_ten <- ten_fun(quo(int_under_agree))
ind3.5_ten <- ten_fun(quo(int_time_agree))
ind3.6_ten <- ten_fun(quo(int_eff_agree))
ind3.7_ten <- ten_fun(quo(int_proc_agree))


ind3.1 <- do.call(cbind, list(ind3.1_ov, ind3.1_loc, ind3.1_sex, ind3.1_hir, ind3.1_opu, ind3.1_ten))
ind3.2 <- do.call(cbind, list(ind3.2_ov, ind3.2_loc, ind3.2_sex, ind3.2_hir, ind3.2_opu, ind3.2_ten))
ind3.3 <- do.call(cbind, list(ind3.3_ov, ind3.3_loc, ind3.3_sex, ind3.3_hir, ind3.3_opu, ind3.3_ten))
ind3.4 <- do.call(cbind, list(ind3.4_ov, ind3.4_loc, ind3.4_sex, ind3.4_hir, ind3.4_opu, ind3.4_ten))
ind3.5 <- do.call(cbind, list(ind3.5_ov, ind3.5_loc, ind3.5_sex, ind3.5_hir, ind3.5_opu, ind3.5_ten))
ind3.6 <- do.call(cbind, list(ind3.6_ov, ind3.6_loc, ind3.6_sex, ind3.6_hir, ind3.6_opu, ind3.6_ten))
ind3.7 <- do.call(cbind, list(ind3.7_ov, ind3.7_loc, ind3.7_sex, ind3.7_hir, ind3.7_opu, ind3.7_ten))

```


```{r}
ind3 <- do.call(rbind, list(ind3.1, ind3.2, ind3.3, ind3.4, ind3.5, ind3.6, ind3.7)) %>% 
  mutate(`#`=1:7) %>% 
  relocate(`#`, .after=last_col())

#ind3

#write_csv(ind3, "output/tables/q3 demographics.csv")

```

# Internal office functionality across Demographics

## Question 3, Table across Demographics

```{r}
ind3_gt <- ind3 %>%
  gt() %>% 
  fmt_percent(2:19,
              rows=c(1:7),
              decimals=1) %>%
  tab_spanner(columns=3:4,
              label="Location") %>%
  tab_spanner(columns=5:6,
              label="Sex") %>%
  tab_spanner(columns=7:10,
              label="Hiring Mechanism") %>%
  tab_spanner(columns=11:15,
              label="Operating Unit") %>% 
  tab_spanner(columns=16:19,
              label = "Tenure")

ind3_gt

#gtsave(ind3_gt, "output/tables/q3 demographics-gt.rtf")

```

```{r}
#created a function to generate the means grouped by each demographic. These means will be used in the plots below. 

means_fun3 <- function (variable) {
datL %>%
  group_by(year, !!variable) %>%
  summarise(q3.1_mn = mean(int_collab_agree, na.rm=T),
            q3.2_mn = mean(int_trust_agree, na.rm=T),
            q3.3_mn = mean(int_tru_b_agree, na.rm=T),
            q3.4_mn = mean(int_under_agree, na.rm=T),
            q3.5_mn = mean(int_time_agree, na.rm=T),
            q3.6_mn = mean(int_eff_agree, na.rm=T),
            q3.7_mn = mean(int_proc_agree, na.rm=T)) %>%
    filter(!is.na(!!variable))
}

q3_loc <- means_fun3(quo(loc_prim_en))
q3_sex <- means_fun3(quo(sex_en)) %>% filter(sex_en == 1 | sex_en == 2)
q3_hir <- means_fun3(quo(hir_mech2)) %>% filter(hir_mech2 == 1 | hir_mech2 == 2 | hir_mech2 == 3 | hir_mech2 == 4)
q3_opu <- means_fun3(quo(op_unit2))
q3_ten <- means_fun3(quo(tenure_en))

```

## Question 3 by Primary Work Station 

```{r}

plot_loc_fun3 <- function (data, variable, demo, title)  {

ggplot(data, aes(year, !!variable, color = as.factor(!!demo))) +
  geom_line(size = 1
            , alpha = .6) +
  scale_y_continuous(limits = c(0, 1), 
                     breaks = seq(0, 1, .2),
                     labels = percent_format(accuracy = 1)) +
  scale_x_continuous(breaks = seq(2020, 2021), 
                     labels = c(2020, 2021),
                     expand = expansion(mult = c (.3, .3))) +
  geom_point(size = 3
             , alpha = .6) +
  labs(x = "", y = "", title = title) +
  theme(legend.title = element_blank(), 
        legend.position = "bottom",
        plot.title = element_text(size = 12, face = "bold"),
        legend.text = element_text(size = 10, face = "bold")) +
  scale_color_manual(values = colors
                     , labels=get_labels(datL$loc_prim_en)) 

}

pq3.1_loc <- plot_loc_fun3(data = q3_loc
                                , variable = quo(q3.1_mn)
                                , demo = quo(loc_prim_en)
                         , title = "Manager promotes\ncollaboration across offices")

pq3.2_loc <- plot_loc_fun3(data = q3_loc
                                , variable = quo(q3.2_mn)
                                , demo = quo(loc_prim_en)
                     , title = "Trust between myself and\ncolleagues within my office")

pq3.3_loc <- plot_loc_fun3(data = q3_loc
                                , variable = quo(q3.3_mn)
                                , demo = quo(loc_prim_en)
                     , title = "Colleagues in other\noffices trust me")

pq3.4_loc <- plot_loc_fun3(data = q3_loc
                                , variable = quo(q3.4_mn)
                                , demo = quo(loc_prim_en)
                     , title = "Understand the day-to-day functions\nof other offices within CPS")

pq3.5_loc <- plot_loc_fun3(data = q3_loc
                                , variable = quo(q3.5_mn)
                                , demo = quo(loc_prim_en)
                     , title = "CPS decisions are\ntaken in a timely fashion")

pq3.6_loc <- plot_loc_fun3(data = q3_loc
                                , variable = quo(q3.6_mn)
                                , demo = quo(loc_prim_en)
                     , title = "CPS decisions are\neffective and transparent")

pq3.7_loc <- plot_loc_fun3(data = q3_loc
                                , variable = quo(q3.7_mn)
                                , demo = quo(loc_prim_en)
                     , title = "New processes\nare efficient")

plots_loc3 <- plot_grid(pq3.1_loc
                      , pq3.2_loc
                      , pq3.3_loc
                      , pq3.4_loc
                      , pq3.5_loc
                      , pq3.6_loc
                      , pq3.7_loc
                      , align = "h")

plots_loc3

#save_plot("output/viz/Q3plots office by location.png", plot = plots_loc3, device = "png", type = "cairo")
```

## Question 3 by Sex

```{r}

plot_sex_fun3 <- function (data, variable, demo, title) {

ggplot(data, aes(year, !!variable, color = as.factor(!!demo))) +
  geom_line(size = 1
            , alpha = .6) +
  scale_y_continuous(limits = c(0, 1), 
                     breaks = seq(0, 1, .2),
                     labels = percent_format(accuracy = 1)) +
  scale_x_continuous(breaks = seq(2020, 2021), 
                     labels = c(2020, 2021),
                     expand = expansion(mult = c (.3, .3))) +
  geom_point(size = 3
             , alpha = .6) +
  labs(x = "", y = "", title = title) +
  theme(legend.title = element_blank(), 
        legend.position = "bottom",
        plot.title = element_text(size = 12, face = "bold"),
        legend.text = element_text(size = 10, face = "bold")) +
  scale_color_manual(values = colors
                     , labels=get_labels(datL$sex_en)) 
  
}

pq3.1_sex <- plot_sex_fun3(data = q3_sex
                                , variable = quo(q3.1_mn)
                                , demo = quo(sex_en)
                         , title = "Manager promotes\ncollaboration across offices")

pq3.2_sex <- plot_sex_fun3(data = q3_sex
                                , variable = quo(q3.2_mn)
                                , demo = quo(sex_en)
                     , title = "Trust between myself and\ncolleagues within my office")

pq3.3_sex <- plot_sex_fun3(data = q3_sex
                                , variable = quo(q3.3_mn)
                                , demo = quo(sex_en)
                     , title = "Colleagues in other\noffices trust me")

pq3.4_sex <- plot_sex_fun3(data = q3_sex
                                , variable = quo(q3.4_mn)
                                , demo = quo(sex_en)
                     , title = "Understand the day-to-day functions\nof other offices within CPS")

pq3.5_sex <- plot_sex_fun3(data = q3_sex
                                , variable = quo(q3.5_mn)
                                , demo = quo(sex_en)
                     , title = "CPS decisions are\ntaken in a timely fashion")

pq3.6_sex <- plot_sex_fun3(data = q3_sex
                                , variable = quo(q3.6_mn)
                                , demo = quo(sex_en)
                     , title = "CPS decisions are\neffective and transparent")

pq3.7_sex <- plot_sex_fun3(data = q3_sex
                                , variable = quo(q3.7_mn)
                                , demo = quo(sex_en)
                     , title = "New processes\nare efficient")

plots_sex3 <- plot_grid(pq3.1_sex
                      , pq3.2_sex
                      , pq3.3_sex
                      , pq3.4_sex
                      , pq3.5_sex
                      , pq3.6_sex
                      , pq3.7_sex
                      , align = "h")

plots_sex3

#save_plot("output/viz/Q3plots office by sex.png", plot = plots_sex3, device = "png", type = "cairo")

```

## Question 3 by Hiring Mechanism 

```{r}

plot_hir_fun3 <- function (data, variable, demo, title) {

ggplot(data, aes(year, !!variable, color = as.factor(!!demo))) +
  geom_line(size = 1
            , alpha = .6) +
  scale_y_continuous(limits = c(0, 1), 
                     breaks = seq(0, 1, .2),
                     labels = percent_format(accuracy = 1)) +
  scale_x_continuous(breaks = seq(2020, 2021), 
                     labels = c(2020, 2021),
                     expand = expansion(mult = c (.3, .3))) +
  geom_point(size = 3
             , alpha = .6) +
  labs(x = "", y = "", title = title) +
  theme(legend.title = element_blank(), 
        legend.position = "bottom",
        plot.title = element_text(size = 12, face = "bold"),
        legend.text = element_text(size = 6, face = "bold")) +
  scale_color_manual(values = colors
                     , labels=get_labels(datL$hir_mech2)) 
  
}

pq3.1_hir <- plot_hir_fun3(data = q3_hir
                                , variable = quo(q3.1_mn)
                                , demo = quo(hir_mech2)
                         , title = "Manager promotes\ncollaboration across offices")

pq3.2_hir <- plot_hir_fun3(data = q3_hir
                                , variable = quo(q3.2_mn)
                                , demo = quo(hir_mech2)
                     , title = "Trust between myself and\ncolleagues within my office")

pq3.3_hir <- plot_hir_fun3(data = q3_hir
                                , variable = quo(q3.3_mn)
                                , demo = quo(hir_mech2)
                     , title = "Colleagues in other\noffices trust me")

pq3.4_hir <- plot_hir_fun3(data = q3_hir
                                , variable = quo(q3.4_mn)
                                , demo = quo(hir_mech2)
                     , title = "Understand the day-to-day functions\nof other offices within CPS")

pq3.5_hir <- plot_hir_fun3(data = q3_hir
                                , variable = quo(q3.5_mn)
                                , demo = quo(hir_mech2)
                     , title = "CPS decisions are\ntaken in a timely fashion")

pq3.6_hir <- plot_hir_fun3(data = q3_hir
                                , variable = quo(q3.6_mn)
                                , demo = quo(hir_mech2)
                     , title = "CPS decisions are\neffective and transparent")

pq3.7_hir <- plot_hir_fun3(data = q3_hir
                                , variable = quo(q3.7_mn)
                                , demo = quo(hir_mech2)
                     , title = "New processes\nare efficient")


plots_hir3 <- plot_grid(pq3.1_hir
                      , pq3.2_hir
                      , pq3.3_hir
                      , pq3.4_hir
                      , pq3.5_hir
                      , pq3.6_hir
                      , pq3.7_hir
                      , align = "h")

plots_hir3

#save_plot("output/viz/Q3plots office by hiring mechanism.png", plot = plots_hir3, device = "png", type = "cairo")
```

## Question 3 by Operating Unit

```{r}

plot_opu_fun3 <- function (data, variable, demo, title) {

ggplot(data, aes(year, !!variable, color = as.factor(!!demo))) +
  geom_line(size = 1
            , alpha = .6) +
  scale_y_continuous(limits = c(0, 1), 
                     breaks = seq(0, 1, .2),
                     labels = percent_format(accuracy = 1)) +
  scale_x_continuous(breaks = seq(2020, 2021), 
                     labels = c(2020, 2021),
                     expand = expansion(mult = c (.3, .3))) +
  geom_point(size = 3
             , alpha = .6) +
  labs(x = "", y = "", title = title) +
  theme(legend.title = element_blank(), 
        legend.position = "bottom",
        plot.title = element_text(size = 12, face = "bold"),
        legend.text = element_text(size = 6, face = "bold")) +
  scale_color_manual(values = colors
                     , labels=get_labels(datL$op_unit2)) 
  
}

pq3.1_opu <- plot_opu_fun3(data = q3_opu
                                , variable = quo(q3.1_mn)
                                , demo = quo(op_unit2)
                         , title = "Manager promotes\ncollaboration across offices")

pq3.2_opu <- plot_opu_fun3(data = q3_opu
                                , variable = quo(q3.2_mn)
                                , demo = quo(op_unit2)
                     , title = "Trust between myself and\ncolleagues within my office")

pq3.3_opu <- plot_opu_fun3(data = q3_opu
                                , variable = quo(q3.3_mn)
                                , demo = quo(op_unit2)
                     , title = "Colleagues in other\noffices trust me")

pq3.4_opu <- plot_opu_fun3(data = q3_opu
                                , variable = quo(q3.4_mn)
                                , demo = quo(op_unit2)
                     , title = "Understand the day-to-day functions\nof other offices within CPS")

pq3.5_opu <- plot_opu_fun3(data = q3_opu
                                , variable = quo(q3.5_mn)
                                , demo = quo(op_unit2)
                     , title = "CPS decisions are\ntaken in a timely fashion")

pq3.6_opu <- plot_opu_fun3(data = q3_opu
                                , variable = quo(q3.6_mn)
                                , demo = quo(op_unit2)
                     , title = "CPS decisions are\neffective and transparent")

pq3.7_opu <- plot_opu_fun3(data = q3_opu
                                , variable = quo(q3.7_mn)
                                , demo = quo(op_unit2)
                     , title = "New processes\nare efficient")


plots_opu3 <- plot_grid(pq3.1_opu
                      , pq3.2_opu
                      , pq3.3_opu
                      , pq3.4_opu
                      , pq3.5_opu
                      , pq3.6_opu
                      , pq3.7_opu
                      , align = "h")

plots_opu3

#save_plot("output/viz/Q3plots office by operating unit.png", plot = plots_opu3, device = "png", type = "cairo")
```

## Question 3 by Tenure

```{r}

plot_ten_fun3 <- function (data, variable, demo, title) {

ggplot(data, aes(year, !!variable, color = as.factor(!!demo))) +
  geom_line(size = 1
            , alpha = .6) +
  scale_y_continuous(limits = c(0, 1), 
                     breaks = seq(0, 1, .2),
                     labels = percent_format(accuracy = 1)) +
  scale_x_continuous(breaks = seq(2020, 2021), 
                     labels = c(2020, 2021),
                     expand = expansion(mult = c (.3, .3))) +
  geom_point(size = 3
             , alpha = .6) +
  labs(x = "", y = "", title = title) +
  theme(legend.title = element_blank(), 
        legend.position = "bottom",
        plot.title = element_text(size = 12, face = "bold"),
        legend.text = element_text(size = 10, face = "bold")) +
  scale_color_manual(values = colors
                     , labels=get_labels(datL$tenure_en)) 
  
}

pq3.1_ten <- plot_ten_fun3(data = q3_ten
                                 , variable = quo(q3.1_mn)
                                 , demo = quo(tenure_en)
                         , title = "Manager promotes\ncollaboration across offices")

pq3.2_ten <- plot_ten_fun3(data = q3_ten
                                 , variable = quo(q3.2_mn)
                                 , demo = quo(tenure_en)
                     , title = "Trust between myself and\ncolleagues within my office")

pq3.3_ten <- plot_ten_fun3(data = q3_ten
                                 , variable = quo(q3.3_mn)
                                 , demo = quo(tenure_en)
                     , title = "Colleagues in other\noffices trust me")

pq3.4_ten <- plot_ten_fun3(data = q3_ten
                                 , variable = quo(q3.4_mn)
                                 , demo = quo(tenure_en)
                     , title = "Understand the day-to-day functions\nof other offices within CPS")

pq3.5_ten <- plot_ten_fun3(data = q3_ten
                                 , variable = quo(q3.5_mn)
                                 , demo = quo(tenure_en)
                     , title = "CPS decisions are\ntaken in a timely fashion")

pq3.6_ten <- plot_ten_fun3(data = q3_ten
                                 , variable = quo(q3.6_mn)
                                 , demo = quo(tenure_en)
                     , title = "CPS decisions are\neffective and transparent")

pq3.7_ten <- plot_ten_fun3(data = q3_ten
                                 , variable = quo(q3.7_mn)
                                 , demo = quo(tenure_en)
                     , title = "New processes\nare efficient")

plots_ten3 <- plot_grid(pq3.1_ten
                      , pq3.2_ten
                      , pq3.3_ten
                      , pq3.4_ten
                      , pq3.5_ten
                      , pq3.6_ten
                      , pq3.7_ten
                      , align = "h")

plots_ten3

#save_plot("output/viz/Q3plots office by tenure.png", plot = plots_ten3, device = "png", type = "cairo")
```
